## ArquivoDePedido
gerador-layouts arquivoDePedido cabecalho CodigoRegistro:string:0:2 IdentificacaoPedidoCliente:string:2:14 CodigoCliente:string:14:24 CnpjCliente:string:24:38 CnpjFornecedor:string:38:52 DataPedido:int32:52:60

gerador-layouts arquivoDePedido detalhe CodigoRegistro:string:0:2 TipoIdentificacaoProduto:int32:2:3 IdentificacaoProduto:string:3:17 CondicaoComercial:string:17:23 EmbalagemProduto:string:23:25 QuantidadePedida:int32:25:36 PrecoProduto:float32:36:52:5 DescontoItem:float32:52:56:2 DescontoRepasseICMS:float32:56:60:2 DescontoComercial:float32:60:64:2

gerador-layouts arquivoDePedido rodape CodigoRegistro:string:0:2 TotalItens:string:2:8 TotalQuantidadePedida:int32:8:19:3 ValorTotalBrutoPedido:float32:19:35:5

---

## ArquivoDeRetorno
gerador-layouts arquivoDeRetorno cabecalho CodigoRegistro:string:0:2 IdentificacaoPedidoCliente:string:2:14 CnpjFornecedor:string:14:28 IdentificacaoInicialPedidoFornecedor:string:28:40 IdentificacaoFinalPedidoFornecedor:string:40:52 PrevisaoDataFaturamento:int32:52:60 PrevisaoDataEntregaMercadoria:int32:60:68 StatusProcessamentoPedido:int32:68:69

gerador-layouts arquivoDeRetorno detalhe CodigoRegistro:string:0:2 TipoIdentificacaoProduto:int32:2:3 IdentificacaoProduto:string:3:17 QuantidadeAtendida:int32:17:28 QuantidadeRecusada:int32:28:39 MotivoRejeicao:string:39:69

gerador-layouts arquivoDeRetorno rodape CodigoRegistro:string:0:2 TotalItensAtendidos:int32:2:8 TotalQuantidadeAtendida:int32:8:19 TotalItensRecusados:int32:19:25 TotalQuantidadeRecusado:int32:25:36

---

## ArquivoNotaFiscal
gerador-layouts arquivoDeNotaFiscal cabecalho CodigoRegistro:string:0:2 IdentificacaoPedidoCliente:string:2:14 CnpjEmissorNotaFiscal:string:14:28 NumeroNotaFiscal:int32:28:34 SerieNotaFiscal:string:34:37 DataEmissaoNotaFiscal:int32:37:45 CodigoFiscalOperacao:int32:45:49 CodigoValorFiscal:int32:49:50 ValorBrutoNotaFiscal:float32:50:65:2 ValorContabilNotaFiscal:float32:65:80:2 ValorTributadoNotaFiscal:float32:80:95:2 ValorOutrasNotaFiscal:float32:95:110:2 BaseICMSRetido:float32:110:125:2 ValorICMSRetido:float32:125:140:2 BaseICMSEntrada:float32:140:155:2 ValorICMSEntrada:float32:155:170:2 BaseIPI:float32:170:185:2 ValorIPI:float32:185:200:2 CgcTransportadora:string:200:214 TipoFrete:string:214:215 ValorFrete:float32:215:230:2 PercentualDescontoComercial:float32:230:236:4 PercentualDescontoRepasseICMS:float32:236:242:4 TotalItens:int32:242:255 TotalUnidades:int32:255:266 TotalNotasParaPedido:int32:266:272 ValorDescontoICMSNormal:float32:272:287:2 ValorDescontoPIS:float32:287:302:2 ValorDescontoCofins:float32:302:317:2 TipoNotaFiscalEntrada:int32:317:319 ChaveAcessoNotaFiscalEletronica:string:319:363

gerador-layouts arquivoDeNotaFiscal produtos CodigoRegistro:string:0:2 TipoIdentificacao:int32:2:3 IdentificacaoProduto:string:3:17 EmbalagemProduto:string:17:19 QuantidadeEmbalagens:int32:19:30 CodigoFiscalOperacao:int32:30:34 SibustituicaoTributaria:string:34:36 PrecoProduto:float32:36:51:5 PercentualDescontoItem:float32:51:57:4 ValorDescontoItem:float32:57:72:5 PercentualDescontoRepasse:float32:72:78:4 ValorDescontoRepasse:float32:78:93:5 PercentualDescontoComercial:float32:93:99:4 ValorDescontoComercial:float32:99:108:5 ValorDespesaAcessorias:float32:108:123:5 ValorDespesaEmbalagem:float32:123:138:5

gerador-layouts arquivoDeNotaFiscal loteFabricacao CodigoRegistro:string:0:2 LoteFabricacao:string:2:22 QuantidadeProduto:int32:22:33 DataValidade:int32:33:41 DataFabricacao:int32:41:49

gerador-layouts arquivoDeNotaFiscal Titulos CodigoRegistro:string:0:2 NumeroTitulo:string:2:12 LocalPagamento:string:12:16 DataVencimento:int32:16:24 ValorBruto:float32:24:39:2 ValorLiquido:float32:39:54:2 DataAntecipacao:int32:54:62 DesscontoAntecipacao:float32:62:66:2 DescontoLimite:float32:66:70:2