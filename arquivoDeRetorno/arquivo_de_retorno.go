package arquivoDeRetorno

import (
	"fmt"
	"time"
)

// Cabeçalho do Retorno do Pedido de Compra
func CabecalhoRetorno(idePedidoCliente string, cnpjFornecedor string, idePedidoFornecedor string, previsaoDataFaturamento time.Time,
											previsaoDataEntrega time.Time, statusProcessamentoPedido int8) string {

	cabecalhoRetorno := fmt.Sprint("01") + "|"
	cabecalhoRetorno += fmt.Sprint(idePedidoCliente) + "|"
	cabecalhoRetorno += fmt.Sprint(cnpjFornecedor) + "|"
	cabecalhoRetorno += fmt.Sprint(idePedidoFornecedor) + "|"
	cabecalhoRetorno += previsaoDataFaturamento.Format("02012006") +  "|"
	cabecalhoRetorno += previsaoDataEntrega.Format("02012006") +  "|"
	cabecalhoRetorno += fmt.Sprint(statusProcessamentoPedido)

	return cabecalhoRetorno
}

// Detalhe do Retorno do Pedido de Compra
func DetalheRetorno(ideProduto string, qtdAtendida int64, qtdRecusada int64, motivoRejeicao string) string {

	cabecalhoRetorno := fmt.Sprint("02") + "|"
	cabecalhoRetorno += fmt.Sprint("1") + "|"
	cabecalhoRetorno += fmt.Sprint(ideProduto) + "|"
	cabecalhoRetorno += fmt.Sprint(qtdAtendida) + "|"
	cabecalhoRetorno += fmt.Sprint(qtdRecusada) + "|"
	cabecalhoRetorno += fmt.Sprint(motivoRejeicao)

	return cabecalhoRetorno
}

// Rodapé do Retorno do Pedido de Compra
func RodapeRetorno(totalItensAtendidos int64,  totalQtdAtendida int64, totalItensRecusados int64, totalQtdRecusado int64)string {

	cabecalhoRetorno := fmt.Sprint("03") + "|"
	cabecalhoRetorno += fmt.Sprint(totalItensAtendidos) + "|"
	cabecalhoRetorno += fmt.Sprint(totalQtdAtendida) + "|"
	cabecalhoRetorno += fmt.Sprint(totalItensRecusados) + "|"
	cabecalhoRetorno += fmt.Sprint(totalQtdRecusado)

	return cabecalhoRetorno
}