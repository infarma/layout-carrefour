package arquivoDeRetorno

type Cabecalho struct {
	CodigoRegistro                       string `json:"CodigoRegistro"`
	IdentificacaoPedidoCliente           string `json:"IdentificacaoPedidoCliente"`
	CnpjFornecedor                       string `json:"CnpjFornecedor"`
	IdentificacaoInicialPedidoFornecedor string `json:"IdentificacaoInicialPedidoFornecedor"`
	IdentificacaoFinalPedidoFornecedor   string `json:"IdentificacaoFinalPedidoFornecedor"`
	PrevisaoDataFaturamento              int32  `json:"PrevisaoDataFaturamento"`
	PrevisaoDataEntregaMercadoria        int32  `json:"PrevisaoDataEntregaMercadoria"`
	StatusProcessamentoPedido            int32  `json:"StatusProcessamentoPedido"`
}
