package arquivoDeRetorno

type Detalhe struct {
	CodigoRegistro           string `json:"CodigoRegistro"`
	TipoIdentificacaoProduto int32  `json:"TipoIdentificacaoProduto"`
	IdentificacaoProduto     string `json:"IdentificacaoProduto"`
	QuantidadeAtendida       int32  `json:"QuantidadeAtendida"`
	QuantidadeRecusada       int32  `json:"QuantidadeRecusada"`
	MotivoRejeicao           string `json:"MotivoRejeicao"`
}