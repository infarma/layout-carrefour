package arquivoDeRetorno

type Rodape struct {
	CodigoRegistro          string `json:"CodigoRegistro"`
	TotalItensAtendidos     int32  `json:"TotalItensAtendidos"`
	TotalQuantidadeAtendida int32  `json:"TotalQuantidadeAtendida"`
	TotalItensRecusados     int32  `json:"TotalItensRecusados"`
	TotalQuantidadeRecusado int32  `json:"TotalQuantidadeRecusado"`
}