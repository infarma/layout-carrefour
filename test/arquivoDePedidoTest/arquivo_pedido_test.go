package arquivoDePedidoTest

import (
	"layout-carrefour/arquivoDePedido"
	"os"
	"testing"
)

func TestGetStruct01(t *testing.T) {

	file, err := os.Open("../testFile/PedidoCompra_200906041320202.5517")
	if err != nil {
		t.Error("não abriu arquivo", err)
	}

	arquivo,err2 := arquivoDePedido.GetStruct(file)
	var cabecalho arquivoDePedido.Cabecalho
	cabecalho.CodigoRegistro = "01"
	cabecalho.IdentificacaoPedidoCliente = "P103040763"
	cabecalho.CodigoCliente = ""
	cabecalho.CnpjCliente = "45543915000424"
	cabecalho.CnpjFornecedor  = "24455677000182"
	cabecalho.DataPedido  = int32(4062009)

	if cabecalho.CodigoRegistro != arquivo.Cabecalho.CodigoRegistro {
		t.Error("CodRegistro não é compativel", err2)
	}

	if cabecalho.IdentificacaoPedidoCliente != arquivo.Cabecalho.IdentificacaoPedidoCliente {
		t.Error("IdentificacaoPedidoCliente não é compativel", err2)
	}

	if cabecalho.CodigoCliente != arquivo.Cabecalho.CodigoCliente {
		t.Error("CodigoCliente não é compativel", err2)
	}

	if cabecalho.CnpjCliente != arquivo.Cabecalho.CnpjCliente {
		t.Error("CnpjCliente não é compativel", err2)
	}

	if cabecalho.CnpjFornecedor != arquivo.Cabecalho.CnpjFornecedor {
		t.Error("CnpjFornecedor não é compativel", err2)
	}

	if cabecalho.DataPedido != arquivo.Cabecalho.DataPedido {
		t.Error("DataPedido não é compativel", err2)
	}

	var detalhe arquivoDePedido.Detalhe
	detalhe.CodigoRegistro = "02"
	detalhe.TipoIdentificacaoProduto = int32(1)
	detalhe.IdentificacaoProduto = "7894916143233"
	detalhe.CondicaoComercial = ""
	detalhe.EmbalagemProduto = "UN"
	detalhe.QuantidadePedida = int32(2)
	detalhe.PrecoProduto = float32(9.03)
	detalhe.DescontoItem = float32(0)
	detalhe.DescontoRepasseICMS = float32(0)
	detalhe.DescontoComercial = float32(0)

	if detalhe.CodigoRegistro != arquivo.Detalhe[0].CodigoRegistro {
		t.Error("CodigoRegistro não é compativel", err2)
	}

	if detalhe.TipoIdentificacaoProduto != arquivo.Detalhe[0].TipoIdentificacaoProduto {
		t.Error("TipoIdentificacaoProduto não é compativel", err2)
	}

	if detalhe.IdentificacaoProduto != arquivo.Detalhe[0].IdentificacaoProduto {
		t.Error("IdentificacaoProduto não é compativel", err2)
	}

	if detalhe.CondicaoComercial != arquivo.Detalhe[0].CondicaoComercial {
		t.Error("CondicaoComercial não é compativel", err2)
	}

	if detalhe.EmbalagemProduto != arquivo.Detalhe[0].EmbalagemProduto {
		t.Error("EmbalagemProduto não é compativel", err2)
	}

	if detalhe.QuantidadePedida != arquivo.Detalhe[0].QuantidadePedida {
		t.Error("QuantidadePedida não é compativel", err2)
	}

	if detalhe.PrecoProduto != arquivo.Detalhe[0].PrecoProduto {
		t.Error("PrecoProduto não é compativel", err2)
	}

	if detalhe.DescontoItem != arquivo.Detalhe[0].DescontoItem {
		t.Error("DescontoItem não é compativel", err2)
	}

	if detalhe.DescontoRepasseICMS != arquivo.Detalhe[0].DescontoRepasseICMS {
		t.Error("DescontoRepasseICMS não é compativel", err2)
	}

	if detalhe.DescontoComercial != arquivo.Detalhe[0].DescontoComercial {
		t.Error("DescontoComercial não é compativel", err2)
	}

	var rodape arquivoDePedido.Rodape
	rodape.CodigoRegistro = "03"
	rodape.TotalItens = int32(3)
	rodape.TotalQuantidadePedida = float32(6)
	rodape.ValorTotalBrutoPedido = float32(91.24)

	if rodape.CodigoRegistro != arquivo.Rodape.CodigoRegistro {
		t.Error("CodigoRegistro não é compativel", err2)
	}

	if rodape.TotalItens != arquivo.Rodape.TotalItens {
		t.Error("TotalItens não é compativel", err2)
	}

	if rodape.TotalQuantidadePedida != arquivo.Rodape.TotalQuantidadePedida {
		t.Error("TotalQuantidadePedida não é compativel", err2)
	}

	if rodape.ValorTotalBrutoPedido != arquivo.Rodape.ValorTotalBrutoPedido {
		t.Error("ValorTotalBrutoPedido não é compativel", err2)
	}
}