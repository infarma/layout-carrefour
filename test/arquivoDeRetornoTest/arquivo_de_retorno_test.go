package arquivoDeRetornoTest

import (
	"layout-carrefour/arquivoDeRetorno"
	"testing"
	"time"
)

func TestCabecalhoRetorno(t *testing.T){
	idePedidoCliente := "123"
	cnpjFornecedor := "22571680000136"
	idePedidoFornecedor := "123"

	src := "2019-10-21T09:56:13.001Z"
	previsaoDataFaturamento, _ := time.Parse(time.RFC3339, src)

	src1 := "2019-10-21T09:56:13.001Z"
	previsaoDataEntrega, _ := time.Parse(time.RFC3339, src1)

	statusProcessamentoPedido := 1

	cabecalhoRetorno := arquivoDeRetorno.CabecalhoRetorno(idePedidoCliente, cnpjFornecedor, idePedidoFornecedor, previsaoDataFaturamento,
																								  		previsaoDataEntrega, int8(statusProcessamentoPedido))

	if len(cabecalhoRetorno) != 45 {
		t.Error("Header Arquivo não tem o tamanho adeguado")
	}else{
		if cabecalhoRetorno != "01|123|22571680000136|123|21102019|21102019|1" {
			t.Error("Header Arquivo não é compativel")
		}
	}
}

func TestDetalheRetorno(t *testing.T){
	ideProduto := "123"
	qtdAtendida := 123
	qtdRecusada := 123
	motivoRejeicao := ""

	detalheRetornop := arquivoDeRetorno.DetalheRetorno(ideProduto, int64(qtdAtendida), int64(qtdRecusada), motivoRejeicao)

	if len(detalheRetornop) != 17 {
		t.Error("Detalhe Retornop não tem o tamanho adeguado")
	}else{
		if detalheRetornop != "02|1|123|123|123|" {
			t.Error("Detalhe Retorno não é compativel")
		}
	}
}

func TestRodapeRetorno(t *testing.T){
	totalItensAtendidos := 345
	totalQtdAtendida := 345
	totalItensRecusados := 345
	totalQtdRecusado := 345

	rodapeRetorno := arquivoDeRetorno.RodapeRetorno(int64(totalItensAtendidos), int64(totalQtdAtendida), int64(totalItensRecusados), int64(totalQtdRecusado))

	if len(rodapeRetorno) != 18 {
		t.Error("Rodape Retorno não tem o tamanho adeguado")
	}else{
		if rodapeRetorno != "03|345|345|345|345" {
			t.Error("Rodape Retorno não é compativel")
		}
	}
}