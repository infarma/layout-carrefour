package arquivoDeNotaFiscalTest

import (
	"layout-carrefour/arquivoDeNotaFiscal"
	"testing"
	"time"
)

func TestCabecalhoNotaFiscal(t *testing.T){

	idePedidoCliente := "77291"
	cnpjEmissorNota := "15156220000186"
	numNotaFiscal := "123"
	serieNotaFiscal := "1"

	src := "2019-10-21T09:56:13.001Z"
	dataEmissao, _ := time.Parse(time.RFC3339, src)

	codFiscalOperacao := 123
	codValorFiscal := 123
	valorBrutoNota := 12.34
	valorContabilNota := 12.34
	valorTributado := 12.34
	valorOutrasNota := 12.34
	baseICMSRetido := 12.34
	valorICMSRetido := 12.34
	baseICMSEntrada := 12.34
	valorICMSEntrada := 12.34
	baseIPI := 12.34
	valorIPI := 12.34
	cGCTransportadora := "15156220000186"
	tipoFrete := "C"
	valorFrete := 12.34
	percentualDesconto := 3
	percentualDescontoRepasseICMS := 4
	totalItens := 14
	totalUnidades := 21
	totalNotasPedido := 2
	valorDescontoICMS := 12.34
	valorDescontoPIS :=  12.34
	valorDescontoCofins :=  12.34
	tipoNotaFiscalEntrada := 55
	chaveAcesso := "123"

	cabecalhoNotaFiscal := arquivoDeNotaFiscal.CabecalhoNotaFiscal(idePedidoCliente, cnpjEmissorNota, numNotaFiscal, serieNotaFiscal, dataEmissao,
																																 int32(codFiscalOperacao), int8(codValorFiscal), valorBrutoNota, valorContabilNota,
																																 valorTributado, valorOutrasNota, baseICMSRetido, valorICMSRetido, baseICMSEntrada,
																																 valorICMSEntrada, baseIPI, valorIPI, cGCTransportadora, tipoFrete, valorFrete, float64(percentualDesconto),
																																 float64(percentualDescontoRepasseICMS), int64(totalItens), int64(totalUnidades), int64(totalNotasPedido),
																																 valorDescontoICMS, valorDescontoPIS, valorDescontoCofins, int8(tipoNotaFiscalEntrada), chaveAcesso)
	if len(cabecalhoNotaFiscal) != 166 {
		t.Error("Cabecalho Nota Fiscal não tem o tamanho adeguado")
	}else{
		if cabecalhoNotaFiscal != "01|77291|15156220000186|123|1|21102019|123|123|12.34|12.34|12.34|12.34|12.34|12.34|12.34|12.34|12.34|12.34|15156220000186|C|12.34|3|4|14|21|2|12.34|12.34|12.34|55|123" {
			t.Error("Cabecalho Nota Fiscal não é compativel")
		}
	}
}

func TestProdutosNotaFiscal(t *testing.T){

	ideProduto := "7896004700441"
	embalagemProduto := "UN"
	qtdEmbalagem := 10
	codFiscalOperacao := 123
	situacaoTributaria := "12"
	precoProduto := 12.34
	percentualDescontoItem := 2
	valorDesconto := 12.34
	percentualDescontoRepasse := 2
	valorDescontoRepasse := 12.34
	percentualComercial := 2
	valorDescontoComercial := 12.34
	valorDespesaAcessorias := 12.34
	valorDespesaEmbalagem := 12.34

	produtosNotaFiscal := arquivoDeNotaFiscal.ProdutosNotaFiscal(ideProduto, embalagemProduto, int32(qtdEmbalagem), int32(codFiscalOperacao), situacaoTributaria, precoProduto,
		float32(percentualDescontoItem), valorDesconto, float64(percentualDescontoRepasse), valorDescontoRepasse,
		float64(percentualComercial), valorDescontoComercial, valorDespesaAcessorias, valorDespesaEmbalagem)

	if len(produtosNotaFiscal) != 73 {
		t.Error("Produtos Nota Fiscal não tem o tamanho adeguado")
	}else{
		if produtosNotaFiscal != "02|1|7896004700441|UN|10|123|12|12.34|2|12.34|2|12.34|2|12.34|12.34|12.34" {
			t.Error("Produtos Nota Fiscal não é compativel")
		}
	}
}


func TestLotefabricacaoProdutosNota(t *testing.T){

	loteFrabricacao := "123"
	qtdProduto := 12

	src := "2019-10-21T09:56:13.001Z"
	dataValidade, _ := time.Parse(time.RFC3339, src)

	src2 := "2019-10-21T09:56:13.001Z"
	dataFabricacao, _ := time.Parse(time.RFC3339, src2)

	lotefabricacaoProdutosNota := arquivoDeNotaFiscal.LotefabricacaoProdutosNota(loteFrabricacao, int64(qtdProduto), dataValidade, dataFabricacao)

	if len(lotefabricacaoProdutosNota) != 27 {
		t.Error("Lote fabricacao Produtos Nota não tem o tamanho adeguado")
	}else{
		if lotefabricacaoProdutosNota != "03|123|12|21102019|21102019" {
			t.Error("Lote fabricacao Produtos Nota não é compativel")
		}
	}
}

func TestTituloNota(t *testing.T){

	numTitulo := "123"
	localPagamento := "123"

	src := "2019-10-21T09:56:13.001Z"
	dataVencimento, _ := time.Parse(time.RFC3339, src)

	valorBruto := 12.34
	valorLiquido := 12.34

	src1 := "2019-10-21T09:56:13.001Z"
	dataAtencipação, _ := time.Parse(time.RFC3339, src1)

	descontoAntecipado := 12.34
	descontoLimite := 12.34

	tituloNota := arquivoDeNotaFiscal.TituloNota(numTitulo, localPagamento, dataVencimento, valorBruto, valorLiquido,
																								dataAtencipação, descontoAntecipado, descontoLimite)

	if len(tituloNota) != 52 {
		t.Error("Titulo Nota não tem o tamanho adeguado")
	}else{
		if tituloNota != "04|123|123|21102019|12.34|12.34|21102019|12.34|12.34" {
			t.Error("Titulo Nota não é compativel")
		}
	}
}