package arquivoDeNotaFiscal

type Cabecalho struct {
	CodigoRegistro                  string  `json:"CodigoRegistro"`
	IdentificacaoPedidoCliente      string  `json:"IdentificacaoPedidoCliente"`
	CnpjEmissorNotaFiscal           string  `json:"CnpjEmissorNotaFiscal"`
	NumeroNotaFiscal                int32   `json:"NumeroNotaFiscal"`
	SerieNotaFiscal                 string  `json:"SerieNotaFiscal"`
	DataEmissaoNotaFiscal           int32   `json:"DataEmissaoNotaFiscal"`
	CodigoFiscalOperacao            int32   `json:"CodigoFiscalOperacao"`
	CodigoValorFiscal               int32   `json:"CodigoValorFiscal"`
	ValorBrutoNotaFiscal            float32 `json:"ValorBrutoNotaFiscal"`
	ValorContabilNotaFiscal         float32 `json:"ValorContabilNotaFiscal"`
	ValorTributadoNotaFiscal        float32 `json:"ValorTributadoNotaFiscal"`
	ValorOutrasNotaFiscal           float32 `json:"ValorOutrasNotaFiscal"`
	BaseICMSRetido                  float32 `json:"BaseICMSRetido"`
	ValorICMSRetido                 float32 `json:"ValorICMSRetido"`
	BaseICMSEntrada                 float32 `json:"BaseICMSEntrada"`
	ValorICMSEntrada                float32 `json:"ValorICMSEntrada"`
	BaseIPI                         float32 `json:"BaseIPI"`
	ValorIPI                        float32 `json:"ValorIPI"`
	CgcTransportadora               string  `json:"CgcTransportadora"`
	TipoFrete                       string  `json:"TipoFrete"`
	ValorFrete                      float32 `json:"ValorFrete"`
	PercentualDescontoComercial     float32 `json:"PercentualDescontoComercial"`
	PercentualDescontoRepasseICMS   float32 `json:"PercentualDescontoRepasseICMS"`
	TotalItens                      int32   `json:"TotalItens"`
	TotalUnidades                   int32   `json:"TotalUnidades"`
	TotalNotasParaPedido            int32   `json:"TotalNotasParaPedido"`
	ValorDescontoICMSNormal         float32 `json:"ValorDescontoICMSNormal"`
	ValorDescontoPIS                float32 `json:"ValorDescontoPIS"`
	ValorDescontoCofins             float32 `json:"ValorDescontoCofins"`
	TipoNotaFiscalEntrada           int32   `json:"TipoNotaFiscalEntrada"`
	ChaveAcessoNotaFiscalEletronica string  `json:"ChaveAcessoNotaFiscalEletronica"`
}