package arquivoDeNotaFiscal

type Produtos struct {
	CodigoRegistro              string  `json:"CodigoRegistro"`
	TipoIdentificacao           int32   `json:"TipoIdentificacao"`
	IdentificacaoProduto        string  `json:"IdentificacaoProduto"`
	EmbalagemProduto            string  `json:"EmbalagemProduto"`
	QuantidadeEmbalagens        int32   `json:"QuantidadeEmbalagens"`
	CodigoFiscalOperacao        int32   `json:"CodigoFiscalOperacao"`
	SibustituicaoTributaria     string  `json:"SibustituicaoTributaria"`
	PrecoProduto                float32 `json:"PrecoProduto"`
	PercentualDescontoItem      float32 `json:"PercentualDescontoItem"`
	ValorDescontoItem           float32 `json:"ValorDescontoItem"`
	PercentualDescontoRepasse   float32 `json:"PercentualDescontoRepasse"`
	ValorDescontoRepasse        float32 `json:"ValorDescontoRepasse"`
	PercentualDescontoComercial float32 `json:"PercentualDescontoComercial"`
	ValorDescontoComercial      float32 `json:"ValorDescontoComercial"`
	ValorDespesaAcessorias      float32 `json:"ValorDespesaAcessorias"`
	ValorDespesaEmbalagem       float32 `json:"ValorDespesaEmbalagem"`
}
