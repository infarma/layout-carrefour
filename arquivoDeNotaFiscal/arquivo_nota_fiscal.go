package arquivoDeNotaFiscal

import (
	"fmt"
	"time"
)

// Cabeçalho do Espelho da Nota Fiscal do Pedido de Compra
func CabecalhoNotaFiscal(idePedidoCliente string, cnpjEmissorNota string, numNotaFiscal string, serieNotaFiscal string, dataEmissao time.Time,
													codFiscalOperacao int32, codValorFiscal int8, valorBrutoNota float64, valorContabilNota float64, valorTributado float64,
													valorOutrasNota float64, baseICMSRetido float64, valorICMSRetido float64, baseICMSEntrada float64, valorICMSEntrada float64,
													baseIPI float64, valorIPI float64, cGCTransportadora string, tipoFrete string, valorFrete float64, percentualDesconto float64,
													percentualDescontoRepasseICMS float64, totalItens int64, totalUnidades int64, totalNotasPedido int64, valorDescontoICMS float64,
													valorDescontoPIS float64, valorDescontoCofins float64, tipoNotaFiscalEntrada int8, chaveAcesso string) string {

	cabecalhoNotaFiscal := fmt.Sprint("01") + "|"
	cabecalhoNotaFiscal += fmt.Sprint(idePedidoCliente) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(cnpjEmissorNota) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(numNotaFiscal) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(serieNotaFiscal) + "|"
	cabecalhoNotaFiscal += dataEmissao.Format("02012006") +  "|"
	cabecalhoNotaFiscal += fmt.Sprint(codFiscalOperacao) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(codValorFiscal) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(valorBrutoNota) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(valorContabilNota) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(valorTributado) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(valorOutrasNota) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(baseICMSRetido) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(valorICMSRetido) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(baseICMSEntrada) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(valorICMSEntrada) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(baseIPI) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(valorIPI) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(cGCTransportadora) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(tipoFrete) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(valorFrete) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(percentualDesconto) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(percentualDescontoRepasseICMS) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(totalItens) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(totalUnidades) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(totalNotasPedido) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(valorDescontoICMS) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(valorDescontoPIS) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(valorDescontoCofins) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(tipoNotaFiscalEntrada) + "|"
	cabecalhoNotaFiscal += fmt.Sprint(chaveAcesso)

	return cabecalhoNotaFiscal
}

// Produtos da Nota Fiscal do pedido de Compra
func ProdutosNotaFiscal(ideProduto string, embalagemProduto string, qtdEmbalagem int32, codFiscalOperacao int32, situacaoTributaria string, precoProduto float64,
												percentualDescontoItem float32, valorDesconto float64, percentualDescontoRepasse float64, valorDescontoRepasse float64,
												percentualComercial float64, valorDescontoComercial float64, valorDespesaAcessorias float64, valorDespesaEmbalagem float64) string {

	produtosNotaFiscal := fmt.Sprint("02") + "|"
	produtosNotaFiscal += fmt.Sprint("1") + "|"
	produtosNotaFiscal += fmt.Sprint(ideProduto) + "|"
	produtosNotaFiscal += fmt.Sprint(embalagemProduto) + "|"
	produtosNotaFiscal += fmt.Sprint(qtdEmbalagem) + "|"
	produtosNotaFiscal += fmt.Sprint(codFiscalOperacao) + "|"
	produtosNotaFiscal += fmt.Sprint(situacaoTributaria) + "|"
	produtosNotaFiscal += fmt.Sprint(precoProduto) + "|"
	produtosNotaFiscal += fmt.Sprint(percentualDescontoItem) + "|"
	produtosNotaFiscal += fmt.Sprint(valorDesconto) + "|"
	produtosNotaFiscal += fmt.Sprint(percentualDescontoRepasse) + "|"
	produtosNotaFiscal += fmt.Sprint(valorDescontoRepasse) + "|"
	produtosNotaFiscal += fmt.Sprint(percentualComercial) + "|"
	produtosNotaFiscal += fmt.Sprint(valorDescontoComercial) + "|"
	produtosNotaFiscal += fmt.Sprint(valorDespesaAcessorias) + "|"
	produtosNotaFiscal += fmt.Sprint(valorDespesaEmbalagem)

	return produtosNotaFiscal
}

// Lote de Fabricação dos Produtos da Nota Fiscal do Pedido de Compra
func LotefabricacaoProdutosNota(loteFrabricacao string, qtdProduto int64, dataValidade time.Time, dataFabricacao time.Time) string {

	lotefabricacaoProdutosNota := fmt.Sprint("03") + "|"
	lotefabricacaoProdutosNota += fmt.Sprint(loteFrabricacao) + "|"
	lotefabricacaoProdutosNota += fmt.Sprint(qtdProduto) + "|"
	lotefabricacaoProdutosNota += dataValidade.Format("02012006") + "|"
	lotefabricacaoProdutosNota += dataFabricacao.Format("02012006")

	return lotefabricacaoProdutosNota
}

// Titulos da Nota Fiscal do Pedido de Compra
func TituloNota(numTitulo string, localPagamento string, dataVencimento time.Time, valorBruto float64, valorLiquido float64,
								dataAtencipação time.Time, descontoAntecipado float64, descontoLimite float64) string {

	tituloNota := fmt.Sprint("04") + "|"
	tituloNota += fmt.Sprint(numTitulo) + "|"
	tituloNota += fmt.Sprint(localPagamento) + "|"
	tituloNota += dataVencimento.Format("02012006") + "|"
	tituloNota += fmt.Sprint(valorBruto) + "|"
	tituloNota += fmt.Sprint(valorLiquido) + "|"
	tituloNota += dataAtencipação.Format("02012006") + "|"
	tituloNota += fmt.Sprint(descontoAntecipado) + "|"
	tituloNota += fmt.Sprint(descontoLimite)

	return tituloNota
}
