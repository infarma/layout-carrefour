package arquivoDeNotaFiscal

type Titulos struct {
	CodigoRegistro       string  `json:"CodigoRegistro"`
	NumeroTitulo         string  `json:"NumeroTitulo"`
	LocalPagamento       string  `json:"LocalPagamento"`
	DataVencimento       int32   `json:"DataVencimento"`
	ValorBruto           float32 `json:"ValorBruto"`
	ValorLiquido         float32 `json:"ValorLiquido"`
	DataAntecipacao      int32   `json:"DataAntecipacao"`
	DesscontoAntecipacao float32 `json:"DesscontoAntecipacao"`
	DescontoLimite       float32 `json:"DescontoLimite"`
}
