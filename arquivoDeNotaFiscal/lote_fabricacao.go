package arquivoDeNotaFiscal

type LoteFabricacao struct {
	CodigoRegistro    string `json:"CodigoRegistro"`
	LoteFabricacao    string `json:"LoteFabricacao"`
	QuantidadeProduto int32  `json:"QuantidadeProduto"`
	DataValidade      int32  `json:"DataValidade"`
	DataFabricacao    int32  `json:"DataFabricacao"`
}
