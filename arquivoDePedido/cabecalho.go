package arquivoDePedido

type Cabecalho struct {
	CodigoRegistro             string `json:"CodigoRegistro"`
	IdentificacaoPedidoCliente string `json:"IdentificacaoPedidoCliente"`
	CodigoCliente              string `json:"CodigoCliente"`
	CnpjCliente                string `json:"CnpjCliente"`
	CnpjFornecedor             string `json:"CnpjFornecedor"`
	DataPedido                 int32  `json:"DataPedido"`
}
