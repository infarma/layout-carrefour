package arquivoDePedido

type Rodape struct {
	CodigoRegistro        string  `json:"CodigoRegistro"`
	TotalItens            int32   `json:"TotalItens"`
	TotalQuantidadePedida float32 `json:"TotalQuantidadePedida"`
	ValorTotalBrutoPedido float32 `json:"ValorTotalBrutoPedido"`
}

