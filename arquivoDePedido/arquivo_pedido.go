package arquivoDePedido

import (
	"bufio"
	"math"
	"os"
	"strconv"
	"strings"
)

type ArquivoDePedido struct {
	Cabecalho Cabecalho `json:"Cabecalho"`
	Detalhe   []Detalhe `json:"Detalhe"`
	Rodape    Rodape    `json:"Rodape"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		vector := strings.Split(string(runes), "|")
		identificador := vector[0]

		if identificador == "01" {
			cabecalho := Cabecalho{}
			cabecalho.CodigoRegistro = vector[0]
			cabecalho.IdentificacaoPedidoCliente = vector[1]
			cabecalho.CodigoCliente = vector[2]
			cabecalho.CnpjCliente = vector[3]
			cabecalho.CnpjFornecedor = vector[4]
			cabecalho.DataPedido  = int32(ConvertStringToInt(vector[5]))

			arquivo.Cabecalho = cabecalho
		} else if identificador == "02" {
			detalhe := Detalhe{}

			detalhe.CodigoRegistro = vector[0]
			detalhe.TipoIdentificacaoProduto = int32(ConvertStringToInt(vector[1]))
			detalhe.IdentificacaoProduto = vector[2]
			detalhe.CondicaoComercial = vector[3]
			detalhe.EmbalagemProduto = vector[4]
			detalhe.QuantidadePedida = int32(ConvertStringToInt(vector[5]))


			precoFloat := strings.Split(vector[6], ".")
			PrecoInteiro := float32(ConvertStringToInt(precoFloat[0]))
			if(len(precoFloat) > 1) {
				PrecoDecimal := (float32(ConvertStringToInt(precoFloat[1]))) / float32(math.Pow10(len(precoFloat[1])))
				detalhe.PrecoProduto = PrecoDecimal + PrecoInteiro
			} else{
				detalhe.PrecoProduto = PrecoInteiro
			}

			DescontoItemFloat := strings.Split(vector[7], ".")
			DescontoItemInteiro := float32(ConvertStringToInt(DescontoItemFloat[0]))
			if(len(DescontoItemFloat) > 1) {
				DescontoItemDecimal := (float32(ConvertStringToInt(DescontoItemFloat[1])))/float32(math.Pow10(len(DescontoItemFloat[1])))
				detalhe.DescontoItem = DescontoItemDecimal + DescontoItemInteiro
			} else{
				detalhe.DescontoItem = DescontoItemInteiro
			}

			DescontoRepasseICMSFloat := strings.Split(vector[8], ".")
			DescontoRepasseICMSInteiro := float32(ConvertStringToInt(DescontoRepasseICMSFloat[0]))
			if(len(DescontoRepasseICMSFloat) > 1) {
				DescontoRepasseICMSDecimal := (float32(ConvertStringToInt(DescontoRepasseICMSFloat[1])))/float32(math.Pow10(len(DescontoRepasseICMSFloat[1])))
				detalhe.DescontoRepasseICMS = DescontoRepasseICMSDecimal + DescontoRepasseICMSInteiro
			} else{
				detalhe.DescontoRepasseICMS = DescontoRepasseICMSInteiro
			}

			DescontoComercialFloat := strings.Split(vector[9], ".")
			DescontoComercialFloatInteiro := float32(ConvertStringToInt(DescontoComercialFloat[0]))
			if(len(DescontoComercialFloat) > 1) {
				DescontoComercialFloatDecimal := (float32(ConvertStringToInt(DescontoComercialFloat[1])))/float32(math.Pow10(len(DescontoComercialFloat[1])))
				detalhe.DescontoComercial = DescontoComercialFloatDecimal + DescontoComercialFloatInteiro
			} else{
				detalhe.DescontoComercial = DescontoComercialFloatInteiro
			}

			arquivo.Detalhe = append(arquivo.Detalhe, detalhe)

		} else if identificador == "03" {
			rodape := Rodape{}
			rodape.CodigoRegistro = vector[0]
			rodape.TotalItens = int32(ConvertStringToInt(vector[1]))

			TotalQuantidadePedidaComercialFloat := strings.Split(vector[2], ".")
			TotalQuantidadePedidaFloatInteiro := float32(ConvertStringToInt(TotalQuantidadePedidaComercialFloat[0]))
			if(len(TotalQuantidadePedidaComercialFloat) > 1) {
				TotalQuantidadePedidaDecimal := (float32(ConvertStringToInt(TotalQuantidadePedidaComercialFloat[1])))/float32(math.Pow10(len(TotalQuantidadePedidaComercialFloat[1])))
				rodape.TotalQuantidadePedida = TotalQuantidadePedidaFloatInteiro + TotalQuantidadePedidaDecimal
			} else{
				rodape.TotalQuantidadePedida = TotalQuantidadePedidaFloatInteiro
			}

			ValorTotalBrutoPedidoFloat := strings.Split(vector[3], ".")
			ValorTotalBrutoPedidoInteiro := float32(ConvertStringToInt(ValorTotalBrutoPedidoFloat[0]))
			if(len(ValorTotalBrutoPedidoFloat) > 1) {
				ValorTotalBrutoPedidoDecimal := (float32(ConvertStringToInt(ValorTotalBrutoPedidoFloat[1])))/float32(math.Pow10(len(ValorTotalBrutoPedidoFloat[1])))
				rodape.ValorTotalBrutoPedido = ValorTotalBrutoPedidoInteiro + ValorTotalBrutoPedidoDecimal
			} else{
				rodape.ValorTotalBrutoPedido = ValorTotalBrutoPedidoInteiro
			}

			arquivo.Rodape = rodape
		}
	}
	return arquivo, err
}

func ConvertStringToInt(valor string) int {
	v, _ := strconv.Atoi(valor)
	return v
}