package arquivoDePedido

type Detalhe struct {
	CodigoRegistro           string  `json:"CodigoRegistro"`
	TipoIdentificacaoProduto int32   `json:"TipoIdentificacaoProduto"`
	IdentificacaoProduto     string  `json:"IdentificacaoProduto"`
	CondicaoComercial        string  `json:"CondicaoComercial"`
	EmbalagemProduto         string  `json:"EmbalagemProduto"`
	QuantidadePedida         int32   `json:"QuantidadePedida"`
	PrecoProduto             float32 `json:"PrecoProduto"`
	DescontoItem             float32 `json:"DescontoItem"`
	DescontoRepasseICMS      float32 `json:"DescontoRepasseICMS"`
	DescontoComercial        float32 `json:"DescontoComercial"`
}

